#!/usr/bin/python

from mod_backend import *
from rvglsessionlog import *

import sys
sistema = SistemaDePuntos()	 # Crea el sistema

#for c in s.listacorredores:
#	poslist = s.GetRacerPositionList(c)
#	pa = si.s.CalcPA(poslist)
#	pp = si.s.CalcPP(poslist)
#	po = si.s.CalcPO(poslist)
#	print c, "PA=%0.2f; PP=%0.3f; PO=%0.2f" % (pa,pp,po)

ruta_archivo_csv = None

try:
	ruta_archivo_csv = sys.argv[1]
except IndexError:
	raise IndexError("Debe especificarse la ruta del archivo csv como argumento.")

print "Cargando archivo csv", ruta_archivo_csv, "..."
sistema.CargarCSV( ruta_archivo_csv )	# Carga el sessionlog

#  SistemaDePuntos tiene 2 atributos internos ("s" y "s_mas8"), los cuales se usa
# uno u otro si la fecha inicio con mas de 8 corredores. Ambos son objetos de tipo
# CalculoDePuntos que por defecto el multiplicador por diferencia de categoria no
# tiene efecto.
categoria = "Professional"	# Cambiar esta variable para establecer otra categoria
						# Tiene que ser una indicada en coches-categorias.csv		
print "Estableciendo categoria:", categoria, "..."
# Establecer categorias
sistema.s.FijarCategoria( categoria )
sistema.s_mas8.FijarCategoria( categoria )

print "Generando datos de la sesion..."
sistema.ProcesarSesion()	# Procesa y genera la lista de corredores, coches y sus respectivas posiciones

# Calcular puntos
print "Calculando puntos..."
sistema.CalcularPuntos()

# Mostrar resumen
print "--- Resumen ---"
sistema.ImprimirResumen()
print "--- Fin de resumen ---"

ruta_archivo_salida = "resultados.csv"
print "Exportando resultados a", ruta_archivo_salida, "..."
sistema.ExportarResultados( ruta_archivo_salida )

print "Fin."
