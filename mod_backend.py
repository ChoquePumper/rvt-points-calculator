import csv
# Dialecto para el csv
class excel_derived:
            delimiter = ','
            quotechar = '"'
            escapechar = None
            doublequote = True
            skipinitialspace = False
            lineterminator = '\n'
            quoting = csv.QUOTE_MINIMAL
			
class excel_cat:
            delimiter = '\t'
            quotechar = '"'
            escapechar = None
            doublequote = True
            skipinitialspace = False
            lineterminator = '\n'
            quoting = csv.QUOTE_MINIMAL

from rvglsessionlog import *

def aDecimal(valor):
	return str(valor).replace(".",",")

class MultiplicadoresCoche:
	class Coche:
		def __init__(self,nombre,multiplicador_base):
			self.nombre = nombre
			self.setMultiplicadorBase(multiplicador_base)
			
		def setMultiplicadorBase(self,multiplicador):
			self.multiplicador_base = float(multiplicador)
			
		def getMultiplicadorBase(self):
			return self.multiplicador_base
		
		def getNombre(self):
			return self.nombre
		
		def setCategoria(self,categoria):	# categoria: objeto Categoria
			self.categoria = categoria
			
		def getCategoria(self):	return self.categoria
			
	class Categoria:
		def __init__(self,nombre,num_valor=0):
			self.nombre = nombre
			self.num_valor = int(num_valor)
			self.coches = dict()
			
		def agregarCoche(self,coche): # coche: objeto Coche
			self.coches[coche.getNombre()] = coche
			coche.setCategoria(self)
			
		def getNombre(self):	return self.nombre
		def getValor(self):	return self.num_valor


	def __init__(self):
		# Categorias:
		self.categorias = dict()
		self.coches = dict()
		self.cache_lista_categorias = []
	
	def agregarCoche(self,nombre,multiplicador,categoria=None):
		coche = MultiplicadoresCoche.Coche(nombre,multiplicador)
		self.coches[nombre] = coche
		if not categoria is None:
			if isinstance(categoria,MultiplicadoresCoche.Categoria):
				categoria.agregarCoche(coche)
			else:
				self.getObjCategoria(categoria).agregarCoche(coche)
			
	def agregarCategoria(self,nombre,valor=0):
		objeto = MultiplicadoresCoche.Categoria(nombre,valor)
		self.categorias[nombre] = objeto
		self.cache_lista_categorias.append(nombre)
		return objeto
		
	def getObjCoche(self,nombre_coche):
		try: return self.coches[nombre_coche]
		except KeyError: return None
	
	def getObjCategoria(self,arg):
		# Si arg es str, entonces busca por nombre
		# Si arg es un objeto Coche, entonces devuelve la caterogia a la que pertenece
		if isinstance(arg,MultiplicadoresCoche.Coche):
			# buscar en las categorias
			return arg.getCategoria()
		else:
			try: return self.categorias[arg]
			except KeyError: return None
				
	def getListaCategorias(self):
		return self.cache_lista_categorias
					 
	@staticmethod
	def cargarCSV(filename):
		f_reader = open(filename)
		csv_reader = csv.reader(f_reader,dialect=excel_cat)
		objeto = MultiplicadoresCoche()
		separador_categoria = "<CATEGORY>"
		categoria_actual = None
		i_linea = 0	# Usado para la informacion del error
		for line in csv_reader:
			i_linea += 1
			cols = len(line)
			try:
				if (	cols<1 or	# Linea en blanco
				  (cols==1 and (len(line[0].strip()) < 0)) or	# Linea con espacios
				  (cols>0 and line[0][0]=="#")	): 	# Caracter de comentario
					continue
				elif line[0].startswith(separador_categoria):
					# Se encontro un separador
					nombre = line[0][len(separador_categoria):]
					valor = line[1]
					categoria_actual = objeto.agregarCategoria(nombre, valor)
				else:	# Coche	
					objeto.agregarCoche(
						line[0],	# Nombre
						#line[1],	#???
						line[2],	# Multiplicador base
						categoria_actual	# Agregar a la categoria
					)
			except IndexError:
				raise ValueError("Error al parsear el archivo de coches-categorias. Vea la linea "+i_linea )
		f_reader.close()
		return objeto
			


# Nuestro sistema de puntos
class EntradaCorredor:
	class Carrera:
		def __init__(self, pos, coche, valido=True):
			self.posicion = int(pos)
			self.coche = coche
			self.valido = valido
		def esValido(self):
			return isinstance(self.valido,bool) and self.valido
		def getCoche(self):
			return self.coche
		def getPosicion(self):
			return self.posicion

	def __init__(self,sistema):
		self.sistema = sistema
		self.finalpos = 0
		self.racername = "---"
		self.car = "---"
		self.multiplier = 1.0
		# Cambio importante: ahora listas de posiciones, coches usados, y chequeos
		#se representan en una sola lista de objetos con dichos datos.
		self.raceslist = []	# Lista de objetos de tipo EntradaCorredor.Carrera
			#self.poslist = None # Lista de posiciones
			#self.carlist = None # Lista de coches
			#self.checklist = None	# Lista de chequeo si es valido o no
		# Datos calculados por CalculoDePuntos
		self.PA = 0
		self.PP = 0.0
		self.CC = 0
		self.PO = 0.0
	
	def AgregarResultado(self, pos, coche, valido=True):
		self.raceslist.append( EntradaCorredor.Carrera(pos,coche,valido) )
		
	def GetCC(self): return self.CC
	def GetPO(self): return self.PO
	def CalcCC(self): # Numero de carreras corridas
	# Precondicion: tener la lista de posiciones
		count = 0
		for res in self.raceslist:	# self.poslist
			if res.getPosicion() > 0: count+=1
		self.CC = count
		return count

	def GetResultado(self, i):
		return self.raceslist[i]
	
	def GetNumeroCarreras(self):
		return len(self.raceslist)
	
	def DeterminarListaDeCoches(self, nombre):
		lista = []
		for res in self.sistema.sesion.results:
			c = res.GetResultByName(nombre)
			if c is not None:
				lista.append(c.car)
			else: lista.append(None)
		##self.carlist = lista
		return lista
	
	def CalcularPuntos(self, calculo): # calculo: objeto CalculoDePuntos
	# Precondicion: tener las listas de posiciones y coches
		self.PA = calculo.CalcPA(self)
		self.PP = calculo.CalcPP(self)
		self.PO = calculo.CalcPO2(self.PA,self.PP)
		
class SistemaDePuntos:
	nombre_csv_categorias = "coches-categorias.csv"
						
	def __init__(self):
		self.multiplicadores = MultiplicadoresCoche.cargarCSV( SistemaDePuntos.nombre_csv_categorias )
		self.s =      CalculoDePuntos( [15,12,10, 7,5,4,2,2,1,1],	self.multiplicadores )
		self.s_mas8 = CalculoDePuntos( [20,16,12,10,8,8,6,4,2,2,1,1], self.multiplicadores )
		self.sesion = None
		self.listacorredores = []
		self.BorrarCaches()
		
	def CargarCSV(self, filename):
		try:
			self.sesion = RVGLsesion.ParseCSV(filename)
		except IOError as e:
			raise IOError('No se pudo leer el archivo "'+filename+'": ' + e.strerror )
		
	def BorrarCaches(self):
		self.cache_datos_csv = None
		self.cache_resumen = None
		
	def ProcesarSesion(self):
		self.listacorredores = []	# Crear una nueva lista
		resultados = self.sesion.results	# lista de los resultados de
												# cada carrera.
		for nombre in self.sesion.listacorredores:
			entrada = EntradaCorredor(self)
			entrada.racername = nombre

			# Cambio: las listas de posiciones, coches, etc. se "juntan".
			for resultado in resultados:
				res_corredor = resultado.GetResultByName(nombre)
				posicion = 0
				coche = None
				valido = False
				if res_corredor is not None: # Si existe resultado para esta carrera
					posicion = res_corredor.pos
					coche = res_corredor.car
					valido = self.DeterminarValidez(coche)
				entrada.AgregarResultado( posicion, coche, valido )		
			
			# Determinar el auto mas usado
			# Nota: se planea quitar este codigo en un futuro
			autos = dict()
			for res in entrada.raceslist:
				car = res.getCoche()
				try:	autos[car] += 1
				except KeyError:
					autos[car]=1
			a = ("---", 0) # Tuple (car, count)
			for auto in autos.iteritems():
				if auto[0] is not None and auto[1] > a[1]:
					a = auto
			entrada.car = a[0] # Setear
			
			# Determinar CC
			entrada.CalcCC()
			
			self.listacorredores.append( entrada )
			
		self.BorrarCaches() # Borrar la cache
	
	def DeterminarValidez(self,coche):
		# Auto valido?
		valido = True
		obj_coche = self.multiplicadores.getObjCoche(coche)
		if obj_coche is not None:
			calculo = self.DeterminarCalculoDePuntos()
			if calculo.fijar_categoria is not None:
				obj_categoria = self.multiplicadores.getObjCategoria( calculo.fijar_categoria )
				coche_categoria = obj_coche.getCategoria()
				if coche_categoria is None or coche_categoria.getValor() > obj_categoria.getValor():
					# Coche sin categoria (custom?) o
					# categoria del coche superior al permitido
					valido = False
				else: 	# Todo OK
					valido = True	
			else: # No tiene categoria fijada
				valido = True
		else:	# Posible auto custom
			valido = False
		return valido

	def CalcularPuntos(self):
		calculo = self.DeterminarCalculoDePuntos()
		for entrada in self.listacorredores:
			entrada.CalcularPuntos(calculo)
		# Borrar la cache
		self.BorrarCaches()
	
	def DeterminarCalculoDePuntos(self):
		calculo = self.s
		# Si el en inicio de la fecha hay mas de 8 corredores, ...
		if self.sesion.GetRaceResults(0).NumResult()>8:
			calculo = self.s_mas8 # se aplica el +8
		return calculo
	
	def GetListaOrdenada(self): # Genera una lista ordenada
		lista = sorted(self.listacorredores,
					key=EntradaCorredor.GetPO,	# Usar el PO como clave para ordenar
					reverse=True)
		for i in range(len(lista)):
			lista[i].finalpos = i+1
		return lista
	
	def GetNumeroCarreras(self):	# Precondicion: tener una sesion cargada
		return self.sesion.NumRaceResults()
		
	def GetNumeroCorredores(self):	# Precondicion: tener una sesion cargada
		return len(self.listacorredores)
	
	def GenerarListasFormatoCSV(self):
		# No vamos a generar varias veces lo mismo si se llama a esta funcion varias veces.
		# Para eso lo guardamos en una cache y lo usamos.
		if self.cache_datos_csv is not None: # Si hay cache del resumen guardado,
			# ... retornar el cache en lugar de generar todo de nuevo.
			return self.cache_datos_csv
		
		total = []
		linea_columnas = ["Pos","Corredor"] #,"Coche"]
		sesion = self.sesion
		for i in range(sesion.NumRaceResults()):
			linea_columnas.append( sesion.GetRaceResults(i).trackname )
		linea_columnas += ["PP","PA","CC","PO"]
		
		total.append( linea_columnas )	# Agregar linea de columnas
		
		lista_ordenada = self.GetListaOrdenada()
		for corredor in lista_ordenada:
			lista_pos = []
			linea_coches = ["",""]	# Espacio para las dos primeras columnas
			ultimo_coche = None	# Ultimo coche usado
			for i in range(self.GetNumeroCarreras()):
				resultado = corredor.GetResultado(i)
				# Posiciones. Reemplazar los 0 por "" (en blanco)
				pos = resultado.getPosicion()
				es_valido = resultado.esValido()
				texto=""
				if pos > 0:
					if not es_valido:
						texto += "'"	# Agrega el apostrofe
					texto += str(pos)	# Agrega la posicion
				lista_pos.append(texto)
				# Agregar los coches usados. Las celdas en blanco significan que el
				# mismo coche se uso en la pista anterior
				coche = resultado.getCoche()
				if not coche == ultimo_coche:
					linea_coches.append(coche)
					ultimo_coche = coche
				else:
					linea_coches.append("")	# Se uso el mismo coche
			# La linea principal: Posicion final, Nombre, posiciones, etc.
			linea = [
				corredor.finalpos,
				corredor.racername,
				#corredor.car	# Reemplazado por linea_coches
			] + lista_pos + [
				aDecimal( corredor.PP ),	# Usar coma en vez de punto
				aDecimal( corredor.PA ),
				corredor.CC,
				aDecimal( corredor.PO )
			]
			# Escribir las lineas
			total.append( linea )
			total.append( linea_coches )
			
		# Guardar en la cache
		self.cache_datos_csv = total
		return total
	
	# Cambio significativo: ahora es obligatorio especificar donde se va a
	#escribir el archivo.
	def ExportarResultados(self, ruta_salida):	# a formato csv
		try:
			ruta_salida = str(ruta_salida)
		except: # Si no se pudo obtener el String
			raise TypeError("Se debe especificar una ruta del archivo valida del csv a generar.")	# ... tirar un error
		lineas = self.GenerarListasFormatoCSV()
		# Crear archivo csv
		try:
			f_writer = open( ruta_salida ,"w")
			csv_writer = csv.writer(f_writer,dialect=excel_derived)
			# Escribir lineas
			for linea in lineas:
				csv_writer.writerow( linea )
			f_writer.close()
		except IOError as e:
			raise IOError('No se pudo escribir en "'+ruta_salida+'": '+e.strerror)
	
	def GenerarResumen(self): # Genera resumen
		if self.cache_resumen is not None:
			return self.cache_resumen
	
		# Se necesita tener los datos de: posicion final en la lista, nombres, puntos (PA, PP, CC, PO).
		def formato_decimal(numero, decimales=3):
			# Poscondicion: el numero decimal sin los ceros adicionales
			texto = bytearray( ("%."+str(int(decimales))+"f") % numero )
			# quitar los ultimos ceros (reemplazarlos por espacios)
			i = len(texto)-1
			while chr(texto[i])=='0':
				texto[i] = ' '
				i -= 1
			if chr(texto[i])=='.' or chr(texto[i])==',': # quitar tambien el punto/coma
				texto[i] = ' '
			return str(texto)
			
		def campo_alineado_izq(texto, max_caracteres=20):
			texto2 = str(texto)
			caracteres_restantes = max_caracteres - len(texto2)
			if caracteres_restantes >= 0:
				# agregar espacios
				texto2 += ("%"+str(caracteres_restantes)+"s")%("")
			else:
				# truncar cadena
				texto2 = texto2[ :caracteres_restantes ]
			return texto2
				
		lista = self.GetListaOrdenada()
		
		# Columnas
		resumen = []
		resumen.append ("Posicion| "+
			campo_alineado_izq("Corredor",22)+"| "+
			campo_alineado_izq("PA",7)+"| "+
			campo_alineado_izq("PP",6)+"| "+
			campo_alineado_izq("CC",3)+"| "+
			campo_alineado_izq("PO",7)+"|"
		)
		# 8+22+7+6+3+7 + 2*5 + 1 = 64 caracteres
		resumen.append ("--------+-----------------------+--------+-------+----+--------+")
		# Mostrar cada corredor
		for corredor in lista:
			resumen.append ("%7d | "%corredor.finalpos +
			campo_alineado_izq( corredor.racername, 22)+"| "+
			"%7s"%formato_decimal(corredor.PA)+"| "+ "%6s"%formato_decimal(corredor.PP)+"| "+
			"%3d"%(corredor.CC)+"| "+ "%7s"%formato_decimal(corredor.PO)+"|"
			)
		
		self.cache_resumen = resumen
		return resumen
		
	def ImprimirResumen(self): # Imprime por salida estandar el resumen
		for linea in self.GenerarResumen():
			print(linea)


# Como calcular los puntos
class CalculoDePuntos:
	def __init__(self, listaPA, multiplicadores=None):
		self.SetListaPA( listaPA )
		self.SetMultiplicadorPorCoche( multiplicadores )
		self.FijarCategoria(None)
	
	def SetListaPA(self, lista):
		# Ejemplo: 12, 10, 7, 5 .
		#El 1ro obtiene 12 puntos, el 2do 10, el 3ro 7, y del 4to para abajo 5
		#El 0 se ignora
		if not isinstance(lista,list):
			raise TypeError("Debe recibir un objeto de tipo lista.")
		elif len(lista) < 1 :
			raise ValueError("La lista debe tener al menos 1 elemento.")
		self.listPA = lista
	
	def SetMultiplicadorPorCoche(self, param):
		self.carmultiplier = param

	def FijarCategoria(self, categoria):
		# Influye en el multiplicador de PA por diferencia de categoria
		if categoria=="":
			categoria = None
		self.fijar_categoria = categoria
			
	def GetValbyPos(self, pos): # Obtener puntos por la posicion
		pos2 = pos-1
		ultimoEnListPA = len(self.listPA)
		if pos == 0: # Se ignora el 0
			return 0
		elif pos > ultimoEnListPA:
			return self.listPA[-1] # El ultimo elemento
		else:
			return self.listPA[pos2]
			
	def GetMultiplierByCar(self, nombre_coche, delta_categoria=None):
		# El 3er parametro aplica al multiplicador por diferencia de categoria
		if self.carmultiplier is None:
			return 1.0
		else:
			coche = self.carmultiplier.getObjCoche(nombre_coche)
			if coche is None:
				return 1.0
			else:
				factor_coche = coche.getMultiplicadorBase()
				factor_categoria = 1.0
				categoria = self.carmultiplier.getObjCategoria(delta_categoria)
				if categoria is not None:
					categoria_coche = coche.getCategoria()
					delta_valor = categoria_coche.getValor() - categoria.getValor()
					if	 delta_valor == -1:	# Una categoria menor
						factor_categoria = 1.2
					elif delta_valor == -2:	# Dos categorias menor
						factor_categoria = 1.5
					elif delta_valor == -3:	# Tres categorias menor
						factor_categoria = 1.8
					elif delta_valor == -4:	# Cuatro categorias menor
						factor_categoria = 2.0
				
				return (factor_coche*factor_categoria)
			#try:
			#	return self.carmultiplier[nombre_coche]
			#except KeyError: return 1.0
	
	# Calculos de puntos:
	def CalcPP(self,corredor): # Calcular PP
		count = 0
		suma = 0
		for i in range(corredor.GetNumeroCarreras()):
			res = corredor.GetResultado(i)
			pos = res.getPosicion()
			es_valido = res.esValido()
			if pos > 0 and es_valido: # Se ignora el 0 y cuando no es valido
				suma += pos
				count += 1
		if count == 0:
			return 0.0
		else:
			return suma/float(count)

	def CalcPA(self,corredor,multiplier=1.0,bonus=0): # Calcular PA
		total = 0
		for i in range(corredor.GetNumeroCarreras()):
			res = corredor.GetResultado(i)
			pos = res.getPosicion()
			coche = res.getCoche()
			es_valido = res.esValido()
			total += (es_valido) * (self.GetValbyPos(pos) * self.GetMultiplierByCar(coche, self.fijar_categoria))
		return total*multiplier+bonus
			
	def CalcPO(self,corredor,multiplierPA=1.0,bonusPA=0): # Calcular PO
		PA = self.CalcPA(corredor,multiplierPA,bonusPA)
		PP = self.CalcPP(corredor)
		return self.CalcPO2(PA,PP)
		
	def CalcPO2(self, PA, PP):
		try:	return PA/PP*0.1
		except ZeroDivisionError: return 0	# Por si PP==0
