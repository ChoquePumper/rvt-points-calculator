
# La idea es reemplazar nombres que son largos por unos cortos para ciertas partes

import csv

class NombresCortos:
	def __init__(self):
		self.dict_nombres = dict() # Elemento principal
		
	def agregarNombreCorto(self, nombre_largo, nombre_corto):
		ya_existe = self.getNombreCorto(nombre_largo)
		if ya_existe is not None :	# Si ya existe, lanzar un error
			raise ValueError("Ya existe un nombre corto para '"+nombre_largo+"': "+ya_existe)
		self.dict_nombres[nombre_largo] = nombre_corto
		
	def getNombreCorto(self, nombre):
		try:
			return self.dict_nombres[nombre]
		except KeyError:
			return None
	
	@staticmethod
	def cargarCSV(filename):
		# El formato del archivo csv debe ser como el del siguiente ejemplo:
		# --- Inicio de ejemplo ---
		# nombrecorto,"Nombre largo 1","Nombre largo 2","Nombre largo 3"
		# NA,"No abrazo"
		# "TH1","Toys in the hood 1","Toys in the hood 1 R"
		# --- Fin de ejemplo ---
		f_csv = open(filename,"r")
		csv_reader = csv.reader(f_csv)
		objeto = NombresCortos()
		for linea in csv_reader:
			if len(linea) > 0: # Se ignoran las lineas vacias
				nombre_corto = linea[0]
				for i in range(1, len(linea)):
					nombre_largo = linea[i]
					objeto.agregarNombreCorto(nombre_largo,nombre_corto)
		f_csv.close()
		return objeto
				

