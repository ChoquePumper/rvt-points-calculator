#
def strToBool(value):
	if value.lower() == "true":
		return True
	elif value.lower() == "false":
		return False
	raise ValueError, "No se puede convertir '%s' a bool."%value

class ResultadoCorredor:
	def __init__(self,pos,playername,car,time,bestlap,finished=True,cheating=True):
		self.pos		= int(pos)
		self.playername	= playername
		self.car		= car
		self.time		= time
		self.bestlap	= bestlap
		self.finished	= strToBool(finished)
		self.cheating	= strToBool(cheating)
	
	@staticmethod
	def ParseCSVline(line):
		return ResultadoCorredor(
			line[0],	# Position
			line[1],	# Player
			line[2],	# Car
			line[3],	# Time
			line[4],	# BestLap
			line[5],	# Finished flag
			line[6]		# Cheating flag
		)

class ResultadosCarrera:
	def __init__(self, trackname, numPlAtStart):
		self.trackname=trackname
		self.numPlAtStart=numPlAtStart
		self.listacorredores = [] # ResultadoCorredor
		
	def AddResult(self,res):
		self.listacorredores.append(res)
		
	def NumResult(self): return len(self.listacorredores)
		
	def GetResult(self,num):
		return self.listacorredores[num]
	
	def GetResultIterator(self):
		return iter(self.listacorredores)
		
	def GetResultByName(self,playername):
		for res in self.GetResultIterator():
			if res.playername == playername:
				return res
		return None
		
	@staticmethod
	def ParseCSVlines(lines):
		i = iter(lines)
		# First line
		first_line = i.next()
		resultados = ResultadosCarrera(
			first_line[1],	# Track name
			first_line[2]	# Number of players at start
		)
		# Second line: column names
		# #,Player,Car,Time,BestLap,Finished
		i.next() # Ignore
		
		# Process the rest
		for line in i:
			res = ResultadoCorredor.ParseCSVline(line)
			resultados.AddResult(res)
		return resultados

class RVGLsesion:
	def __init__(self, version, a,b, datetime, host, difficulty, laps, pickups):
		self.version = version	# String: RVGL version
		self.a = a	# String: P2P/???
		self.b = b	# String: server/???
		self.sessiondatetime = datetime	# String:
		self.host = host				# String:
		self.difficulty = difficulty	# String: Arcade, etc
		self.laps = laps				# Int: number of laps
		self.pickups = pickups			# Boolean: pickups enabled?
		# Initialize lists
		self.results = []	# Each one is a RaceResults object (ResultadosCarrera)
		self.listacorredores = []

	def __str__(self):
		return "<RVGLsesion log, %s, %s, %d race results >" % (
			self.sessiondatetime,
			self.version,
			len(self.results)
		)

	def AddRaceResults(self,res):
		self.results.append(res)
		
	def NumRaceResults(self): return len(self.results)
		
	def GetRaceResults(self,num):
		return self.results[num]

	def ProcessRacersList(self):
		for res in self.results:
			for c in res.GetResultIterator():
				if not c.playername in self.listacorredores:
					self.listacorredores.append( c.playername )
					
	def GetRacerPositionList(self, racer):
		poslist = []
		for res in self.results:
			r = res.GetResultByName(racer)
			if (r is None) or (not r.finished):
				poslist.append(0)	# El 0 indica que no termino.
			else:
				poslist.append(r.pos)
		return poslist

	@staticmethod
	def ParseCSV(filename):
		# Working on v19.0330a
		import csv
		f = open(filename)
		csv_reader = csv.reader(f)
		line1 = csv_reader.next()	# Version
		line2 = csv_reader.next()	# Session
		
		session = RVGLsesion(
			# First line sample:
			# Version,"RVGL 19.0330a","P2P","Server"
			version = line1[1],
			a = line1[2],	# Connection type? e.g.: P2P
			b = line1[3],	# ??	# Someone help me pls
			# Second line sample:
			# Session,"04/08/19 02:02:32","LOBBY NAME","Arcade",3,"false"
			datetime = line2[1],	# Date & time (from the start?)
			host = line2[2],		# Lobby name
			difficulty = line2[3],	# Difficulty: Arcade, Simulation, etc.
			laps = int(line2[4]),	# Laps
			pickups = line2[5]		# Pickups enable flag
		)
		# Parsing race results
		lines = []	# Each line represents each racer
		for line in csv_reader:
			if line[0] == "Session":
				continue	# En caso de que el jugador cambie de coche, se crea otra
							#linea Session. Ignorar linea y juntar todo.
			if line[0] == "Results" and len(lines)>1:	# Separator for each race
				# Load results (from the previous lines) to session.
				session.AddRaceResults( ResultadosCarrera.ParseCSVlines(lines) )
				lines = []	# Start a new list
			lines.append(line)
		f.close()
		if len(lines)>1:	# Load the last one
			session.AddRaceResults( ResultadosCarrera.ParseCSVlines(lines) )
		
		session.ProcessRacersList()
		return session

	
	
