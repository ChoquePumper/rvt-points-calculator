#!/usr/bin/lua
-- Nota: no ejecutar directamente este script. Hacerlo desde python con el modulo
--"lupa", ya que lua necesitará acceder a los objetos y clases de python.

-- Se requiere las librerias de interfaz grafica "iup". "iuplua" es el binding para
--lua.
-- Me pareció una buena opción. Funciona en Windows y GNU/Linux (GTK). F por Mac :(
require "iuplua"
require "iupluacontrols"


local function list_copy_to_table(L)
	local t,i = {},1
	for item in python.iter(L) do
		if isinstance( item,list ) then -- Si es una lista
			t[i] = list_copy_to_table( item )
		else
			t[i] = item
		end
		i = i+1
	end
	return t
end

-- Importar de python los nombres necesarios para poder manejarlos en lua.
--Python hara el trabajo cuando sean invocados.
SistemaDePuntos = python.eval("SistemaDePuntos")
MultiplicadoresCoche = python.eval("MultiplicadoresCoche")
isinstance = python.eval("isinstance")
list = python.eval("list")
dict = python.eval("dict")
nombres_cortos = python.eval("nombres_cortos") -- Nota: no es requerido
if type(nombres_cortos)=="userdata" and tostring(nombres_cortos)=="None" then
	nombres_cortos = nil
end

-- Nota: En Lua 5.3, con la llegada del subtipo integer para el tipo number,
--lo que retorne python ignorando si es número entero en Lua 5.3 va a aparecer
--siempre como decimal. Este comportamiento puede resultar molesto cuando se
--trata de mostrar numeros enteros de python porque por defecto lo muestra
--por ejemplo como "1.0" en lugar de "1".
if _VERSION >= "Lua 5.3" then
	--math.tointeger = function(valor) return valor; end
	local old_tonumber = tonumber
	tonumber = function(valor, ...)
		return math.tointeger(valor) or old_tonumber(valor, ...)
	end
end


local sistema
local nombre_archivo
local datos_hoja	-- Basicamente sería el contenido del csv en forma de tablas de lua
local categoria_fijada


---------------- Para operar con el GUI ---------------------
MainWindow = {
	create = function()
		local self = {
			sistema = nil,
			nombre_archivo = nil,
			datos_hoja = nil,
			celdas_combinadas = {},
				-- Ya que iup no ofrece una función para descombinar todas las celdas,
				--tengo que guardar las celdas combinadas para luego descombinarlas.
			ih = nil,
			mat = iup.matrix {numcol=2, numlin=4, heightdef=7,
				resizematrix="YES", font="Bahnschrift, 11"
			},
			boton = iup.button { title="Boton", size="48"} ,
			
			btnAbrirCSV = iup.button{title="Abrir CSV", size=48},
			txtCategoria = iup.list{ dropdown="YES", editbox="YES", size=100},
			btnProcesarCalcular = iup.button{title="Procesar y calcular", size=72, active="NO"},
			btnExportarResultados = iup.button{title="Exportar resultados", size=72, active="NO"}
		}
		
		-- Establecer los elementos de la lista de categorias
		local multiplicadores_coche = MultiplicadoresCoche.cargarCSV("coches-categorias.csv")
		local lista_categorias = list_copy_to_table(multiplicadores_coche.getListaCategorias())
		for i,v in ipairs(lista_categorias) do
			self.txtCategoria[i] = v
		end
		
		self.btnAbrirCSV.action = function(self2, ...)
			self:OnBtnAbrirCSV(...)
			return iup.DEFAULT
		end
		
		function self.btnProcesarCalcular.action(self2, ...)
			self:procesarYCalcular()
			self:descombinarCeldas()
			self:cargarDatosHoja()
			self.mat.redraw = "ALL"
			return iup.DEFAULT
		end
		
		function self.btnExportarResultados.action(self2, ...)
			self:OnBtnExportarResultados(...)
			return iup.DEFAULT
		end
		
		self.ih = iup.dialog {
			iup.vbox {
				self.mat,
				iup.hbox {
					self.btnAbrirCSV, 
					iup.space{size=8},
					iup.label{title="Fijar categoria:"},self.txtCategoria,
					self.btnProcesarCalcular, self.btnExportarResultados,
					NULL
					; alignment="ACENTER"
				},
				NULL
			}
			; title="RVT Points Calculator", size="480x240", shrink="YES",
				dropfilestarget="YES"--, droptypes="TEXT,FILE,text/csv,UTF8_STRING,COMPOUND_TEXT,BITMAP,DIB"
		}
		-- Callback: cuando recibe un archivo vía arrastrar/soltar
		self.ih.dropfiles_cb = function(self2, filename, num, x, y)
			self:abrirCSV(filename)
			return iup.DEFAULT
		end

		setmetatable(self,{__index=MainWindow})
		return self
	end
	,
	show = function(self, ...)
		self.ih:show(...)
		return self
	end
	,
	
	abrirCSV = function(self, filename)
		sistema = SistemaDePuntos()	-- Crear sistema
		sistema:CargarCSV(filename)
		
		self:procesarYCalcular()
		
		nombre_archivo = filename
		
		local num_pistas = tonumber(sistema:GetNumeroCarreras())
		local num_corredores = tonumber(sistema:GetNumeroCorredores())
		--print(num_corredores)
		self:prepararHoja(num_corredores, num_pistas)
		self:cargarDatosHoja()
		
		self.mat.redraw = "ALL"
		--iup.Refresh(self.mat)
		self.btnProcesarCalcular.active = "YES"
		self.btnExportarResultados.active = "YES"
	end
	,
	procesarYCalcular = function(self)
		-- Establecer categorias 
		local categoria = self.txtCategoria.value
		sistema.s:FijarCategoria( categoria )
		sistema.s_mas8:FijarCategoria( categoria )
		categoria_fijada = categoria
		
		sistema:ProcesarSesion()
		sistema:CalcularPuntos()
		
		local datos = sistema:GenerarListasFormatoCSV()
		datos_hoja = list_copy_to_table(datos)
	end
	,
	exportarResultados = function(self, filename)
		print('Exportando resultados a "'..filename..'"...')
		sistema.ExportarResultados(filename)
	end
	,
	prepararHoja = function(self, n_corredores, n_pistas)
		local matrix = self.mat
		self:descombinarCeldas() -- Descombinar todas las celdas
		
		--** Preparar variables **--
		-- Columnas: PF, Corredor, [Pista1, Pista2, ...], PP, PA, CC, PO
		local numcol = 2 + n_pistas + 4
		local numlin = tonumber(n_corredores*2)	-- El corredor y los coches que usó.
		
		matrix["CLEARVALUE"]="CONTENTS"	-- Limpiar
		matrix["CLEARATTRIB"]="ALL"
		
		-- Establecer filas y columnas
		matrix.numcol = numcol
		matrix.numlin = numlin
		
		--** Establecer apariencia **--
		matrix.flat = "YES"
		matrix.height0 = 15	-- Mostrar la cabecera de columnas y setear tamaño.
		
		--* Establecer fuentes. Esto se hace antes para ajustar el ancho de columnas.
		--matrix["FONT0:*"]
		matrix["FONT0:1"] = "Bahnschrift, Bold 8"	-- Cabecera PF
		matrix["FONT*:1"] = "Bahnschrift, Bold 11"	-- Columna PF
		for i=3, numcol-4 do	-- Columnas de cada pista
			matrix["FONT*:"..i] = "Bahnschrift Light, Bold Italic 11"
		end
		for i=numcol-3, numcol do
			matrix["FONT0:"..i] = "Bahnschrift, Bold 14"
		end
		matrix["FONT*:"..numcol] = "Bahnschrift, Bold 12"	-- Columna PO
		
		--* Ajustar el ancho de las columnas
		-- PF				Corredor
		matrix.width1=12;	matrix.width2=4*19 -- aprox. 19 caracteres
		for i=3, numcol-4 do	-- Columnas de cada pista
			matrix["WIDTH"..i] = 4*4
		end
		-- Columnas PA y PP
		matrix["WIDTH"..numcol-3], matrix["WIDTH"..numcol-2] = 4*8, 4*8
		
		-- Columnas CC y PO
		matrix["WIDTH"..numcol-1], matrix["WIDTH"..numcol] = 4*3, 4*12
		
		--* Establecer colores. Nota: BGCOLOR=fondo, FGCOLOR=texto
		matrix.bgcolor = "#2D3F43"
		-- Cabecera de columnas
		matrix["BGCOLOR0:*"], matrix["FGCOLOR0:*"] = "#1D282B", "#FFD966"
		-- Columna 1 (PF)
		matrix["BGCOLOR*:1"]="#FFD966"; matrix["FGCOLOR*:1"]="#906000"
		-- Columna 2 (Corredor)
		matrix["FGCOLOR*:2"]="#BDD7EE"
		-- Columnas de cada pista
		for i=3, numcol-4 do	
			matrix["FGCOLOR*:"..i] = "#5BADA9"
		end
		for i=numcol-3, numcol-1 do -- Cabeceras de PA, PP y CC
			matrix["FGCOLOR0:"..i] = "#FFFFFF"
		end
		-- Columnas PA y PP
		matrix["BGCOLOR*:"..numcol-3], matrix["FGCOLOR*:"..numcol-3] = "#1D282B", "#AFABAB"
		matrix["BGCOLOR*:"..numcol-2], matrix["FGCOLOR*:"..numcol-2] = "#1D282B", "#AFABAB"
		-- Cabeceras CC y PO
		matrix["BGCOLOR0:"..numcol-1], matrix["BGCOLOR0:"..numcol] = "#181717", "#262626"
		-- Columnas CC y PO
		matrix["BGCOLOR*:"..numcol-1], matrix["FGCOLOR*:"..numcol-1] = "#181717", "#AFABAB"
		matrix["BGCOLOR*:"..numcol], matrix["FGCOLOR*:"..numcol] = "#262626", "#FFD966"
		
		--* Alinear texto
		matrix["ALIGNMENT2"] = "ALEFT" -- En columna Corredor
		
		--** Establecer algunos textos
		matrix:setcell(0,1, "PF")
		matrix:setcell(0,2, "Corredor")
		matrix:setcell(0,numcol-3, "PA")
		matrix:setcell(0,numcol-2, "PP")
		matrix:setcell(0,numcol-1, "CC")
		matrix:setcell(0,numcol, "PO")
		
		-- Refrescar
		iup.Refresh(matrix)
	end
	,
	cargarDatosHoja = function(self)
		-- La fila 1 de datos_hoja contiene las columnas
		self.mat["CLEARVALUE"]="CONTENTS"
		
		local function esColPos(i_col) return i_col == 1; end
		local function esColPA(i_col) return i_col == self.mat.numcol-3; end
		local function esColCC(i_col) return i_col == self.mat.numcol-1; end
		local function esColPO(i_col) return i_col == tonumber(self.mat.numcol); end
		
		-- Mostrar los datos
		for i_fila=2, #datos_hoja do
			local fila = datos_hoja[i_fila]
			local ultimo_coche = ""
			local columna_coche = 0
			for i_col,valor in ipairs(fila) do
				local valor_a_mostrar;
				if ( esColPos(i_col) or esColCC(i_col) ) and i_fila%2==0 and _VERSION=="Lua 5.3" then
					-- En Lua 5.3 se agregó un subtipo del tipo "number" para diferenciar numeros enteros
					-- Si quiero que la posición se vea como por ejemplo "1" y no "1.0", tengo que hacer esto.				
					valor_a_mostrar = tonumber(valor)
					
				elseif esColPA(i_col) or esColPO(i_col) then
					-- Cortar el string para que solo se muestre una cierta cantidad de decimales.
					local i_coma = valor:find(",")
					if i_coma then
						local decimals = valor:len() - i_coma
						if decimals > 3 then
							valor_a_mostrar = valor:sub(1,i_coma+3)
						end
					end
				end
				-- Combinar celdas donde se eligió el mismo coche
				if i_fila>1 and i_fila%2==1 and i_col>2 and i_col<self.mat.numcol-3 then
					self.mat["ALIGN"..(i_fila-1)..":"..(i_col)] = ":ALEFT"
					if valor~="" then
						if columna_coche > 2 then
							self:combinarCeldas(i_fila-1,columna_coche, i_fila-1,i_col-1)
						end
						ultimo_coche = valor
						columna_coche = i_col
						-- valor puede ser None. En ese caso, reemplazar por un texto vacío
						if type(valor)=="userdata" and tostring(valor)=="None" then
							valor_a_mostrar = ""
						end
					end
				end
				self.mat:setcell(i_fila-1, i_col, valor_a_mostrar or valor)
			end
			self:combinarCeldas(i_fila-1,columna_coche, i_fila-1,self.mat.numcol-4)
		end
		
		-- Mostrar los nombres de las pistas (con nombres cortos en lo posible)
		local fila_columnas = datos_hoja[1]
		for i_columna=3, self.mat.numcol-4 do
			local nombre_pista = fila_columnas[i_columna]
			local nombre_corto
			if nombres_cortos then -- usar los nombres cortos solamente si está disponible
				nombre_corto = nombres_cortos.getNombreCorto(nombre_pista)
					-- puede devolver None, que en lua se traduce como nil
			end
			self.mat:setcell(0, i_columna, nombre_corto or nombre_pista)
		end
	end
	,
	combinarCeldas = function(self, lin, col, lin2, col2)
		self.mat["MERGE"..(lin)..":"..(col)] = ""..(lin2)..":"..(col2)
		-- Guardar
		self.celdas_combinadas[#self.celdas_combinadas+1] = string.format("%d:%d", lin,col)
	end
	,
	descombinarCeldas = function(self) -- descombina todo
		for i,valor in ipairs(self.celdas_combinadas) do
			self.mat["MERGESPLIT"] = valor
			self.celdas_combinadas[i] = nil
		end
		--self.celdas_combinadas = {}
	end
	,
	-- Eventos
	OnBtnAbrirCSV = function(self)
		local fdlg = iup.filedlg{
			dialogtype	= "OPEN",
			filter	= "*.csv",
			parentdialog = self.ih
		}
		fdlg:popup()
		
		if fdlg.status=="0" then
			local filename = fdlg.value
			self:abrirCSV(filename)
		end
		
	end
	,
	OnBtnExportarResultados = function(self)
		local filename_inicial = "resultados.csv"
		local categoria = categoria_fijada
		if categoria and categoria:len()>0 then
			filename_inicial = "resultados-"..categoria..".csv"
		end
		
		local fdlg = iup.filedlg{
			dialogtype	= "SAVE",
			filter	= "*.*",
			file	= filename_inicial,
			parentdialog = self.ih,
		}
		fdlg:popup()
		
		if not (fdlg.status=="-1") then
			local filename = fdlg.value
			self:exportarResultados(filename)
		end
		
	end
}

local main_dlg = MainWindow.create()
main_dlg:prepararHoja(2,2)
main_dlg:show()

-- Para probar
--main_dlg:abrirCSV("session_2020-05-08_20-45-43.csv")

if (iup.MainLoopLevel()==0) then
	iup.MainLoop()
end
