#!/usr/bin/python

import sys
import lupa
from lupa import LuaRuntime

lua = None
if sys.platform == "win32":
	lua = LuaRuntime(encoding=None)
else:
	lua = LuaRuntime()	# (unpack_returned_tuples=True)
loadfile = lua.eval("loadfile")

# Importar los nombres para que lua los pueda usar.
from mod_backend import *
from rvglsessionlog import *
from nombres_cortos import *

nombres_cortos = None
try: nombres_cortos = NombresCortos.cargarCSV("nombres_cortos.csv")
except IOError as e: # No es requerido, asi que solo muestro un aviso
	print "Aviso: error al leer el archivo de nombres cortos: "+e.message

# Que el script lua haga el trabajo. ;)
# La verdad es que no encontre librerias de interfaz grafica tan portables y ligeras
#como "iup" que tiene sus bindings para lua y aparentemente no hay para python.
#Asi que pense en esta solucion algo rebuscada (Python->Lua).
# P.D.: Lua es genial. Desearia haberlo aprendido antes de hacer este sistema en python.
res = loadfile( "./rvt-gui.lua") 
if isinstance(res,tuple):
	raise Exception("Error al cargar el script lua: "+res[1])

res()
	# Nota: no ejecutar directamente el script lua. Hacerlo desde
	#aqui, ya que lua necesitara acceder a los objetos y clases
	#de python.
